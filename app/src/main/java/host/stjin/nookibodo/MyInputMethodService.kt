package host.stjin.nookibodo

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.inputmethodservice.InputMethodService
import android.provider.Settings
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import net.gotev.speech.*
import java.util.*


@ExperimentalStdlibApi
class MyInputMethodService : InputMethodService(),
    ActivityCompat.OnRequestPermissionsResultCallback {
    private var keyboardView: View? = null
    private var MAXCHARSALLOWED = 23

    lateinit var nookibodoPrefs: NookibodoPrefs


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (requestCode == 20032020) {
            if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                onWindowShown()
            } else {
                onWindowShown()
            }
        }
    }

    private fun checkForMicrophonePermissions(): Boolean {
        val speechProgressText = keyboardView?.findViewById<TextView>(R.id.progress_text)!!

        val permission = ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.RECORD_AUDIO
        )

        if (permission != PackageManager.PERMISSION_GRANTED) {
            Log.i("MyInputMethodService", "Permission to record denied")
            speechProgressText.text =
                applicationContext.resources.getString(R.string.mic_permissions_required)
            speechProgressText.setTextColor(ContextCompat.getColor(applicationContext, R.color.red))
            speechProgressText.setOnClickListener {
                launchSettings()
            }
            return false
        } else {
            speechProgressText.setTextColor(
                ContextCompat.getColor(
                    applicationContext,
                    R.color.leafgreen
                )
            )
            speechProgressText.text = applicationContext.resources.getString(R.string.say_something)
            return true
        }
    }

    override fun onWindowHidden() {
        super.onWindowHidden()
        // prevent memory leaks when activity is destroyed
        Speech.getInstance().shutdown()
    }

    override fun onWindowShown() {
        super.onWindowShown()
        Speech.init(this, packageName)

        if(checkForMicrophonePermissions()) {
            nookibodoPrefs = NookibodoPrefs(applicationContext)
            startListening()
            showToFinishSentences(keyboardView)
            showSentenceHistory()
            setOnClickListener()
        }
    }

    private fun setOnClickListener() {
        val nookibodo_keyboard_switcher =
            keyboardView?.findViewById<ImageView>(R.id.nookibodo_keyboard_switcher)!!

        val nookibodo_keyboard_settings =
            keyboardView?.findViewById<ImageView>(R.id.nookibodo_keyboard_settings)!!

        nookibodo_keyboard_settings.setOnClickListener {
            launchSettings()
        }

        nookibodo_keyboard_switcher.setOnClickListener {
            val imeManager: InputMethodManager =
                applicationContext.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imeManager.showInputMethodPicker()
        }

    }


    private fun launchSettings() {
        requestHideSelf(0)
        val intent = Intent()
        intent.setClass(this, MainActivity::class.java)
        intent.flags = (Intent.FLAG_ACTIVITY_NEW_TASK
                or Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED
                or Intent.FLAG_ACTIVITY_CLEAR_TOP)
        startActivity(intent)
    }

    fun addLinebreaks(input: String, maxLineLength: Int): String {
        val tok = StringTokenizer(input, " ")
        val output = java.lang.StringBuilder(input.length)
        var lineLen = 0
        while (tok.hasMoreTokens()) {
            val word: String = tok.nextToken()
            if (lineLen + word.length > maxLineLength) {
                output.append("||")
                lineLen = 0
            }
            output.append("$word ")
            lineLen += word.length
        }
        return output.toString()
    }

    private fun startListening() {
        //val speechProgressView = keyboardView?.findViewById<SpeechProgressView>(R.id.progress)!!
        val speechProgressText = keyboardView?.findViewById<TextView>(R.id.progress_text)!!
        speechProgressText.text = applicationContext.resources.getString(R.string.say_something)

        try {
            // you must have android.permission.RECORD_AUDIO granted at this point
            Speech.getInstance().startListening(object : SpeechDelegate {
                override fun onStartOfSpeech() {
                    Log.i("speech", "speech recognition is now active")
                }

                override fun onSpeechRmsChanged(value: Float) {
                    Log.d("speech", "rms is now: $value")
                }


                override fun onSpeechPartialResults(results: List<String>) {
                    val str = StringBuilder()
                    for (res in results) {
                        str.append(res).append(" ")
                    }

                    Log.i(
                        "speech",
                        "partial result: $results"
                    )



                    speechProgressText.text = results.toString().replace("[", "").replace("]", "")


                }

                override fun onSpeechResult(result: String) {
                    Log.i("speech", "result: $result")

                    speechProgressText.text = result
                    // Split up the sentence into smaller sentences of max MAXCHARSALLOWED
                    val sentencesToSend = addLinebreaks(result, MAXCHARSALLOWED)
                    val sentencesList = ArrayList<String>()
                    sentencesList.addAll(sentencesToSend.split("||"))

                    if (result != "") {
                        if (sentencesList.size > 1) {
                            nookibodoPrefs.setLastSentenceValues(sentencesList)
                            sendText(sentencesList[0])
                        } else {
                            nookibodoPrefs.clearFinishLastSentenceValues()
                            sendText(sentencesList[0])
                        }
                    }
                }
            })
        } catch (exc: SpeechRecognitionNotAvailable) {
            Log.e("speech", "Speech recognition is not available on this device!")

            speechProgressText.text =
                applicationContext.resources.getString(R.string.speech_recognition_required)
            speechProgressText.setTextColor(ContextCompat.getColor(applicationContext, R.color.red))
            speechProgressText.setOnClickListener {
                SpeechUtil.redirectUserToGoogleAppOnPlayStore(this)
            }

            // You can prompt the user if he wants to install Google App to have
            // speech recognition, and then you can simply call:
            //
            // SpeechUtil.redirectUserToGoogleAppOnPlayStore(this);
            //
            // to redirect the user to the Google App page on Play Store
        } catch (exc: GoogleVoiceTypingDisabledException) {
            Log.e("speech", "Google voice typing must be enabled!")

            speechProgressText.text =
                applicationContext.resources.getString(R.string.google_voice_typing_must_be_enabled)
            speechProgressText.setTextColor(ContextCompat.getColor(applicationContext, R.color.red))
            speechProgressText.setOnClickListener {
                val intent =
                    Intent(Settings.ACTION_INPUT_METHOD_SETTINGS)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
            }
        }
    }

    @ExperimentalStdlibApi
    private fun sendText(text: String) {
        // Add text to history
        nookibodoPrefs.addSentenceToHistory(text)


        val ic = currentInputConnection
        ic.commitText(text, 1)

        ic.sendKeyEvent(
            KeyEvent(
                KeyEvent.ACTION_DOWN,
                KeyEvent.KEYCODE_ENTER
            )
        )
        ic.sendKeyEvent(
            KeyEvent(
                KeyEvent.ACTION_UP,
                KeyEvent.KEYCODE_ENTER
            )
        )

        requestHideSelf(0)
    }

    override fun onDestroy() {
        super.onDestroy()
        // prevent memory leaks when activity is destroyed
        Speech.getInstance().shutdown()
    }

    override fun onCreateInputView(): View {
        // get the KeyboardView and add our Keyboard layout to it
        keyboardView = layoutInflater.inflate(R.layout.keyboard_view, null)


        return keyboardView!!
    }

    private fun showToFinishSentences(keyboardView: View?) {
        val lastSentence = nookibodoPrefs.getLastSentenceValues()
        val chipGroup =
            keyboardView?.findViewById<ChipGroup>(R.id.progress_finish_sentence_chipgroup)!!
        val progress_finish_sentence_LL =
            keyboardView.findViewById<LinearLayout>(R.id.progress_finish_sentence_LL)!!

        // Clear all views
        chipGroup.removeAllViews()

        var sentencecount = 0
        if (lastSentence != null) {

            if (lastSentence.size > 1) {
                progress_finish_sentence_LL.visibility = View.VISIBLE

                for (sentence in lastSentence) {
                    // If the sentence is NOT 0, get the text
                    // skip the first sentence because it has been sent already.
                    // It will still be shown in the history
                    if (sentencecount != 0) {
                        Log.e("finish speech", sentence)

                        val chip = Chip(chipGroup.context)
                        chip.text = sentence

                        // necessary to get single selection working
                        chip.isClickable = true
                        chip.isCheckable = true
                        chip.setOnClickListener {
                            sendText(chip.text as String)
                        }
                        chipGroup.addView(chip)
                    }

                    sentencecount++
                }
            } else {
                progress_finish_sentence_LL.visibility = View.GONE
            }


        }
    }


    private fun showSentenceHistory() {
        val history = nookibodoPrefs.getSentenceHistory()
        val chipGroup =
            keyboardView?.findViewById<ChipGroup>(R.id.progress_recent_sentence_chipgroup)!!
        val progress_history_sentence_LL =
            keyboardView?.findViewById<LinearLayout>(R.id.progress_history_sentence_LL)!!
        // Clear all views
        chipGroup.removeAllViews()

        if (history != null) {
            if (history.size > 0) {
                progress_history_sentence_LL.visibility = View.VISIBLE

                for (sentence in history) {
                    Log.e("speech history", sentence)

                    val chip = Chip(chipGroup.context)
                    chip.text = sentence

                    // necessary to get single selection working
                    chip.isClickable = true
                    chip.isCheckable = true
                    chip.setOnClickListener {
                        sendText(chip.text as String)
                    }
                    chipGroup.addView(chip)
                }
            } else {
                progress_history_sentence_LL.visibility = View.GONE
            }

        }
    }
}